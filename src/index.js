import React, { useCallback, useState } from 'react';
// import PropTypes from 'prop-types';  // TODO dep or devDependency ?
import AsyncSelectCreatable from 'react-select/async-creatable';
import debounce from 'debounce-promise';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { components } from 'react-select';

import { DEFAULT_DELAY } from './config';
import pianoStyles from './assets/scss/main.scss';  // TODO refactor to be provided as prop to Select component ?

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faPencilAlt,
  faSpinner,
} from '@fortawesome/free-solid-svg-icons';
library.add(faPencilAlt, faSpinner);

const propIsFn = (prop) => !!prop && typeof prop === 'function';

const getStyles = ({error}) => {
  return {
    container: (provided) => {
      return { ...provided, flex: 1 };
    },
    control: (base, state) => {
      const borderColor = error ? '#dc3545' : state.isFocused ? pianoStyles.inputFocusBorderColor : '#ced4da';
      const boxShadowColour = error ? '#f6ccd0' : pianoStyles.inputFocusBoxShadow;
      const boxShadow = state.isFocused ? error ? `0 0 0 .2rem ${boxShadowColour}` : boxShadowColour : base.boxShadow;
      return {
        ...base,
        boxShadow: boxShadow,
        borderColor,
        '&:hover': {
          borderColor,
        }
      }
    },
    option: (base, state) => {
      const { isFocused, isSelected } = state;
      let [color, backgroundColor] = [base.color, base.backgroundColor];
      if (isFocused)
        [color, backgroundColor] = ['#fff', pianoStyles.primary];
      else if (isSelected)
        [color, backgroundColor] = [pianoStyles.primary, '#fff'];
      return {
        ...base,
        color,
        backgroundColor,
      }
    },
    multiValue: (base, { data }) => ({
      ...base,
      backgroundColor: pianoStyles.primaryLight,
    }),
    multiValueLabel: (base, { data }) => ({
      ...base,
      color: pianoStyles.primaryLightYiq,
    }),
    multiValueRemove: (base, { data }) => ({
      ...base,
      ':hover': {
        color: '#fff',
        backgroundColor: '#dc3545',
      }
    }),
    placeholder: base => ({
      ...base,
      opacity: 0.6
    }),
    menuPortal: styles => ({
      ...styles,
      zIndex: 9999
    }),
  }
}

export const Select = React.forwardRef(({ loadOptions, delay, onBeginLoad, allowCreation=false, onEscape, onConfirm, onInputChange, menuPortalTarget, value, error, ...props }, ref) => {
  let _components = {};
  if (!!props.components) {
    _components = props.components;
  } else {
    if (props.onEdit) {
      _components.DropdownIndicator = _props => {
        _props.innerProps.onMouseDown = props.onEdit;
        _props.innerProps.onTouchEnd = props.onEdit;
        return (
          <components.DropdownIndicator {..._props} >
            <FontAwesomeIcon icon="pencil-alt" size="sm"/>
          </components.DropdownIndicator>
        )
      }
    } else {
      _components.DropdownIndicator = () => null;
      _components.IndicatorSeparator = () => null;
    }
  }

  const [isLoading, setIsLoading] = useState(false)
  const [inputValue, setInputValue] = useState("")
  const [_error, setError] = useState(null);
  const [menuOpen, setMenuOpen] = useState(false);
  const openMenuOnFocus = true; // todo: pretty sure this is correct since we always load default options?

  const combinedError = error || _error;
  const stylesProps = {error: combinedError};
  const selectStyles = propIsFn(props.styles) ? props.styles(stylesProps) : getStyles(stylesProps);

  const debouncedLoadOptions = useCallback(
    debounce(loadOptions, delay || DEFAULT_DELAY), [loadOptions, delay]
  );

  const debouncedWithLoading = inputValue => {
    if (isLoading === false) {
      if (onBeginLoad) onBeginLoad()
      setIsLoading(true)
    }
    return debouncedLoadOptions(inputValue).then(options => {
      setIsLoading(false)
      setError(null)
      return options
    }).catch(err => {
      setError(err)
      return []
    })
  }

  const onKeyDown = e => {
    if (e.key === 'Escape') onEscape && onEscape();
    if (e.key === 'Enter' && !menuOpen) onConfirm && onConfirm();
  }

  const noOptionsMessage = () => {
    if (propIsFn(props.noOptionsMessage))
      return props.noOptionsMessage();
    return inputValue.trim().length ? props.noResultsMessage || "No Results!" : null;
  }

  const isValidNewOption = () => {
    if (propIsFn(props.isValidNewOption))
      return props.isValidNewOption(inputValue);
    return allowCreation;
  }

  const handleInputChange = (input) => {
    setInputValue(input);
    if (propIsFn(onInputChange))
      onInputChange(input);
  }

  return (<React.Fragment>
    <AsyncSelectCreatable
      {...props}
      ref={ref}
      isClearable
      autoFocus={props.autoFocus === undefined ? true : props.autoFocus}
      isValidNewOption={isValidNewOption}
      isMulti={props.allowMulti || false}
      isLoading={isLoading}
      isDisabled={props.disabled}
      isOptionDisabled={option => !!option.disabled}
      loadingMessage={e => {
        return isLoading ?
          <span>loading <FontAwesomeIcon icon="spinner" size="sm" spin /></span>
          : undefined
      }}
      loadOptions={debouncedWithLoading}
      openMenuOnFocus={openMenuOnFocus}
      openMenuOnClick={false}
      autoSize={false}
      value={value}
      placeholder={props.placeholder || ""}
      noOptionsMessage={noOptionsMessage}
      onInputChange={handleInputChange}
      onKeyDown={onKeyDown}
      onMenuOpen={() => setMenuOpen(true)}
      onMenuClose={() => setMenuOpen(false)}
      components={_components}
      menuPortalTarget={menuPortalTarget ? document.getElementById(menuPortalTarget) : undefined}
      styles={selectStyles}
    />
    {combinedError && <div className="invalid-feedback d-block">
      {combinedError}
    </div>}
  </React.Fragment>)
});
// TODO Select.propTypes = { ... }

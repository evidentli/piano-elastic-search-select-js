# elastic-search-select

> A component that searches with elasticsearch and renders a select component with the results

[![NPM](https://img.shields.io/npm/v/elastic-search-select.svg)](https://www.npmjs.com/package/elastic-search-select) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install -D git+https://bitbucket.org/evidentli/piano-elastic-search-select-js.git
```

## Usage

```jsx
import React from 'react'

import ElasticSearchSelect from 'piano-elastic-search-select-js'
import 'elastic-search-select/dist/index.css'

const Example = () => {
  const loadOptions = 
    inputValue => fetch(url + inputValue)
                  .then(res => res.json())
                  .then(data => data.results.map(e=>({
                    value: e.concept_code,
                    label: e.concept_code + " " + e.concept_name
                  })));
  return (
    <ElasticSearchSelect
      loadOptions={loadOptions}
    />
  )
}
```

## License

MIT ©

import React, { useState } from 'react'

import { Select } from 'elastic-search-select'
import 'elastic-search-select/dist/index.css'

const App = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [_error, setError] = useState("");
  const error = _error.length > 0 ? _error : null;
  const loadOptions = inputValue => {
    let suffix = "";
    if (inputValue) {
      //suffix = `&${inputValue.split(/\s+/).map(i=>`query=${i}`).join('&')}`;
      suffix = `&query=${inputValue}`
    }
    return fetch(`http://localhost:8080/http://dev.evidentli.com:8181/omop/concept?filters=domain_id:condition&filters=invalid_reason:null${suffix}`)
      .then(res => res.json())
      .then(js => {
        console.log(js);
        const options = js.results.map(e => ({
          value: e.concept_code,
          label: e.concept_code + " " + e.concept_name
        }));
        console.log(options);
        setIsLoading(false);
        return options;
      })
  }
  return <>
    <div>{isLoading ? "loading..." : "not loading..."}</div>
    <div><input type="text" placeholder="error" value={_error} onChange={e => setError(e.target.value)} /></div>
    <Select loadOptions={loadOptions} onBeginLoad={() => setIsLoading(true)} onEdit={e => console.log(e)} error={error} />
  </>
}

export default App
